/* Kullanilacak kutuphaneler eklenir... */
var babelify = require('babelify');
var browserify = require('browserify');
var glob = require('glob');
var gulp = require('gulp');
var cleanCss = require('gulp-clean-css');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');

/* Gelistirme icin kullanilacak bagimliliklar tanimlanir... */
var dependencies = [
    'react',
    'react-dom'
];

/* React harici olarak kullanilan javascript dosyalarinin birlestirilmesi saglanabilir... */
gulp.task('CreateMainScripts', function () {
    return gulp.src('./src/main-scripts/*.js')
        .pipe(concat('main-app.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./dist/scripts/'));
});

gulp.task('CreateMainStyles', function () {
    return gulp.src('./src/main-styles/*.css')
        .pipe(concatCss('main-style.css'))
        .pipe(cleanCss())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./dist/styles/'));
});

/* Gelistirme tamamlandiktan sonra ortam "gelistirme" olarak isaretlenir... */
gulp.task('apply-prod-environment', function () {
     process.env.NODE_ENV = 'development';
    // process.env.NODE_ENV = 'production';
});

/* React ve ReactDOM scriptlerinin kaynak kodlarinin uretilmesi icin task olusturulur... */
gulp.task('CreateVendorJs', function () {
    browserify({ require: dependencies })
        .bundle()
        .pipe(source('react-vendor.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./dist/scripts/'));
});
gulp.task('CreateCustomReactJs', function () {
    // var reactFiles = glob.sync('./src/react-scripts/*.js');
    browserify({
        // entries: reactFiles
        entries: './src/react-scripts/app.js'
    })
        .external("react")
        .external("react-dom")
        .transform("babelify", { presets: ["es2015", "react"] })
        .bundle()
        .pipe(source('react-components.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./dist/scripts/'));
});

/* Birlikte calisacak tasklar olusturulur... */
/* Production ortaminda react-vendor.min.js dosyasinin olusturulmasi... (Bu bir defa calistirilabilir!) */
gulp.task('CreateProductionVendorJs', ['apply-prod-environment', 'CreateVendorJs']);
/* main-style.min.css ve main-app.min.js dosyasinin olusturulmasi... */
gulp.task('CreateMainStylesAndScripts', ['CreateMainScripts', 'CreateMainStyles']);
/* react-bundle.min.js dosyasinin production ortaminda olusturulmasi (react scriptlerinin birlestirilmesi!) */
gulp.task('CreateProductionReactComponentJs', ['apply-prod-environment', 'CreateCustomReactJs']);