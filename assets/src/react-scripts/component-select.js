import React from 'react'
import {countries} from './dummy-data';
import {cities} from './dummy-data';
import {dealers} from './dummy-data';

export class DataHelper {
  static GetCity(cityId) {
    return cities.filter((element) => element.id == cityId);
  }
  static GetCities(countryId) {
    return cities.filter((element) => element.countryid == countryId);
  }
  static GetAddresses(cityId) {
    return dealers.filter((element) => element.cityid == cityId);
  }
  static GetAllPositions() {
    return dealers.map(function (element) {
      return { lat: element.lat, lng: element.lng, name: element.name, phonenumber: element.phonenumber, address: element.address }
    });
  }
}

export default class GoogleMapApp extends React.Component {
  componentWillMount() {
    this.setState(
      {
        ZoomLevel: 8,
        CenterLat: DataHelper.GetAllPositions()[0].lat,
        CenterLng: DataHelper.GetAllPositions()[0].lng
      });
  }

  ChangeArea(lat, lng) {
    this.setState(
      {
        ZoomLevel: 12,
        CenterLat: lat,
        CenterLng: lng
      });
  }

  render() {
    return (
      <div className="container">
        <div className="starter-template">
          <h1>{this.props.header}</h1>
          <div className="row">
            <CascadingSelectionBox AddressIsClicked={this.ChangeArea.bind(this) }  />
            <MapBox zoomLevel={this.state.ZoomLevel} lat={this.state.CenterLat} lng={this.state.CenterLng} />
          </div>
        </div>
      </div>
    );
  }
}
export class MapBox extends React.Component {
  componentWillReceiveProps(newProps) {
    if (this.state != null) {
      let _position = new google.maps.LatLng(newProps.lat, newProps.lng);
      this.state.map.setCenter(_position);
      this.state.map.setZoom(newProps.zoomLevel);
    }
  }

  componentDidMount() {
    var mapOptions = {
      center: new google.maps.LatLng(this.props.lat, this.props.lng),
      zoom: this.props.zoomLevel
    };

    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    var infoWindow = new google.maps.InfoWindow();
    DataHelper.GetAllPositions().forEach(function (item) {
      if (item.lat != undefined && item.lng != undefined && item.name != undefined) {
        var marker = new google.maps.Marker({ position: new google.maps.LatLng(item.lat, item.lng), map: map });
        google.maps.event.addListener(marker, 'click', function () {
          infoWindow.close();
          infoWindow.setContent('<p><strong>' + item.name + '</strong></p><p>' + item.phonenumber + '</p><p>' + item.address + '</p>');
          infoWindow.open(map, marker);
        });
      }
    });
    this.setState({ map: map });
  }

  render() {
    return (
      <div className="col-md-9">
        <div id="map" style={{ height: '564px' }}></div>
      </div>
    )
  }
}

export class CascadingSelectionBox extends React.Component {
  componentWillMount() {
    let _cities = DataHelper.GetCities(countries[0].id);
    this.setState(
      {
        countryId: countries[0].id,
        cities: _cities,
        cityId: _cities[0].id
      }
    );
  }

  OnCountryChange(event) {
    let _cities = DataHelper.GetCities(event.target.value);
    this.setState(
      {
        cities: _cities,
        cityId: _cities[0].id
      }
    );
  }

  onCityChange(event) {
    let _city = DataHelper.GetCity(event.target.value)[0];
    this.setState(
      {
        cityId: _city.id
      }
    );
  }

  render() {
    let listItems = [];
    let data = countries;
    data.forEach(function (item) {
      listItems.push(<SelectListItem key={item.id} data={item} />)
    }.bind(this));

    return (
      <div className="col-md-3">
        <div className="row">
          <div className="col-md-6">
            <div className="form-group">
              <label for="country-list">Ülke seçiniz: </label>
              <select onChange={this.OnCountryChange.bind(this) } defaultValue={this.state.countryId}  className="form-control">
                {listItems}
              </select>
            </div>
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <label for="country-list">Şehir seçiniz: </label>
              <CityBox data={this.state.cities} onchange={this.onCityChange.bind(this) } />
            </div>
          </div>
        </div>
        <div className="row">
          <AddressBox cityId={this.state.cityId} AddressIsClicked={this.props.AddressIsClicked} />
        </div>
      </div>
    );
  }
};

export class CityBox extends React.Component {
  render() {
    let listItems = [];
    let data = this.props.data;
    data.forEach(function (item) {
      listItems.push(<SelectListItem key={item.id} data={item} />)
    }.bind(this));

    return (
      <select onChange={this.props.onchange} className="form-control">
        {listItems}
      </select>
    );
  }
};

export class AddressBox extends React.Component {
  componentWillMount() {
    this.setState({ addresses: DataHelper.GetAddresses(this.props.cityId) });
  }
  componentWillReceiveProps(newProps) {
    this.setState({ addresses: DataHelper.GetAddresses(newProps.cityId) });
  }
  render() {
    let addressItems = [];
    let data = this.state.addresses;
    data.forEach(function (item) {
      addressItems.push(<AddressItem key={item.id} data={item} AddressIsClicked={this.props.AddressIsClicked} />)
    }.bind(this));
    return (
      <div className="col-md-12 address-list">
        {addressItems}
      </div>
    );
  }
}

class SelectListItem extends React.Component {
  render() {
    return (
      <option value={this.props.data.id}>{this.props.data.name}</option>
    );
  }
}

class AddressItem extends React.Component {
  render() {
    return (
      <div className="well" onClick={this.props.AddressIsClicked.bind(this, this.props.data.lat, this.props.data.lng) }>
        <p>{this.props.data.name}</p>
        <p>
          <span className="glyphicon glyphicon-phone-alt" aria-hidden="true"></span>
          <span className="supplier-info-label">{this.props.data.phonenumber}</span>
        </p>
        <p>
          <span className="glyphicon glyphicon-envelope" aria-hidden="true"></span>
          <span className="supplier-info-label">{this.props.data.address}</span>
        </p>
      </div>
    );
  }
}