export const countries = [
  { id: "02", name: "Almanya" },
  { id: "01", name: "Türkiye" },
  { id: "03", name: "Suudi Arabistan" },
  { id: "04", name: "Katar" },
  { id: "05", name: "Fransa" }
];

export const cities = [
  { id: "06", countryid: "01", name: "Ankara" },
  { id: "16", countryid: "01", name: "Bursa" },
  { id: "34", countryid: "01", name: "İstanbul" },
  { id: "35", countryid: "01", name: "İzmir" },
  { id: "100", countryid: "02", name: "Berlin" },
  { id: "101", countryid: "02", name: "Münih" },
  { id: "102", countryid: "02", name: "Stutgart" },
  { id: "103", countryid: "03", name: "Mekke" },
  { id: "104", countryid: "04", name: "Doha" },
  { id: "105", countryid: "05", name: "Paris" }
]

export const dealers = [
  { id: "01", cityid: "100", name: "1. Berlin Şubesi", phonenumber: "0(444)555-55-55", address: "Berlin Sanayi Bölgesi", lat: 52.5588359, lng: 13.2862487 },
  { id: "02", cityid: "101", name: "1. Münih Şubesi", phonenumber: "0(444)000-00-00", address: "Münih Sanayi Bölgesi", lat: 48.1731628, lng: 11.5444149 },
  { id: "03", cityid: "102", name: "1. Stutgart Şubesi", phonenumber: "0(444)111-11-11", address: "Stutgart Sanayi Bölgesi", lat: 48.7901502, lng: 9.1809517 },
  { id: "04", cityid: "06", name: "1. Ankara Şubesi", phonenumber: "0(232)111-11-11", address: "Ankara Sanayi Bölgesi", lat: 39.9035662, lng: 32.4825713 },
  { id: "05", cityid: "16", name: "1. Bursa Şubesi", phonenumber: "0(111)222-22-22", address: "Bursa Sanayi Bölgesi", lat: 40.2218557, lng: 28.8922033 },
  { id: "06", cityid: "34", name: "1. İstanbul Şubesi", phonenumber: "0(222)333-33-33", address: "İstanbul Sanayi Bölgesi", lat: 41.0055005, lng: 28.7319867 },
  { id: "07", cityid: "35", name: "1. İzmir Şubesi", phonenumber: "0(333)444-44-44", address: "İzmir Sanayi Bölgesi", lat: 38.4178607, lng: 26.9396754 },
  { id: "08", cityid: "103", name: "1. Mekke Şubesi", phonenumber: "0(555)666-66-66", address: "Mekke Sanayi Bölgesi", lat: 21.4362767, lng: 39.7064583 },
  { id: "09", cityid: "104", name: "1. Doha Şubesi", phonenumber: "0(666)777-77-77", address: "Doha Sanayi Bölgesi", lat: 25.2894348, lng: 51.3893709 },
  { id: "10", cityid: "105", name: "1. Paris Şubesi", phonenumber: "0(777)888-88-88", address: "Paris Sanayi Bölgesi", lat: 48.8589507, lng: 2.2775168 }
]