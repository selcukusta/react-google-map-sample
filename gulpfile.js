/* Kullanilacak kutuphaneler eklenir... */
var babelify = require('babelify');
var browserify = require('browserify');
var glob = require('glob');
var gulp = require('gulp');
var cleanCss = require('gulp-clean-css');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');
var bsConfig = require("gulp-bootstrap-configurator");
/* Gulp Config */
/* Gelistirme icin kullanilacak bagimliliklar tanimlanir... */
var dependencies = [
    'react',
    'react-dom'
];
gulp.task('apply-prod-environment', function () {
     process.env.NODE_ENV = 'development';
    // process.env.NODE_ENV = 'production';
});

/* React ve ReactDOM scriptlerinin kaynak kodlarinin uretilmesi icin task olusturulur... */
gulp.task('CreateVendorJs', function () {
    browserify({ require: dependencies })
        .bundle()
        .pipe(source('react-vendor.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./assets/dist/scripts/'));
});
gulp.task('CreateCustomReactJs', function () {
    // var reactFiles = glob.sync('./src/react-scripts/*.js');
    browserify({
        // entries: reactFiles
        entries: './assets/src/react-scripts/app.js'
    })
        .external("react")
        .external("react-dom")
        .transform("babelify", { presets: ["es2015", "react"] })
        .bundle()
        .pipe(source('react-components.js'))    
        .pipe(buffer())
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./assets/dist/scripts/'));
});
gulp.task('make-bootstrap-js', function(){
  return gulp.src("./bootstrap.config.json")
    .pipe(bsConfig.js({
      compress: true,
      bower: false,
      name: 'bootstrap.min.js'
    }))
    .pipe(gulp.dest("./assets/dist/scripts")); 
});
gulp.task('make-bootstrap-css', function(){
  return gulp.src("./bootstrap.config.json")
    .pipe(bsConfig.css({
      compress: true,
      bower: false,
      name: 'bootstrap.min.css'
    }))
    .pipe(gulp.dest("./assets/dist/styles"));
});
 gulp.task('watch', function () {
     gulp.watch(['./assets/src/react-scripts/*.js'], ['CreateProductionReactComponentJs']);
 });
/*************** GLOBAL *********************/
/* Production ortaminda react-vendor.min.js dosyasinin olusturulmasi... (Bu bir defa calistirilabilir!) */
gulp.task('CreateProductionVendorJs', ['apply-prod-environment', 'CreateVendorJs']);
gulp.task('CreateBootstrapAssets', ['make-bootstrap-css', 'make-bootstrap-js']);
/* react-bundle.min.js dosyasinin production ortaminda olusturulmasi (react scriptlerinin birlestirilmesi!) */
gulp.task('CreateProductionReactComponentJs', ['apply-prod-environment', 'CreateCustomReactJs']);